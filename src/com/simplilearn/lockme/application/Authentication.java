package com.simplilearn.lockme.application;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

import com.simplilearn.lockme.model.UserCredentials;
import com.simplilearn.lockme.model.Users;


public class Authentication {

	private static Scanner keyboard;
	private static Scanner input;
	private static Scanner lockerInput;
	
	private static PrintWriter output;
	private static PrintWriter lockerOutput;
	
	private static Users users;
	private static UserCredentials userCredentials;
	
	public static void signInOptions() {
		
		System.out.println("1.  Registartion");
		System.out.println("2.  Login");
		int option = keyboard.nextInt();
		switch(option) {
		case 1 :
			registerUser();
			break;
		case 2 :
			loginUser();
			break;
		default :
			System.out.println("Please Select 1 or 2");
			break;
		
		}
		
		
		keyboard.close();
		input.close();
		
	}
	
	public static void registerUser() {
		System.out.println("==============================================");
		System.out.println("*                                            *");
		System.out.println("*      Welcome to Registration Page          *");
		System.out.println("*                                            *");
		System.out.println("==============================================");
		
		System.out.println("Enter Username :");
		String username=keyboard.next();
		users.setUsername(username);
		System.out.println("Enter Password:" );
		String password=keyboard.next();
		users.setPassword(password); 
		
		output.println(users.getUsername());
		output.println(users.getPassword());
		
		System.out.println("User Registration Successful");
		output.close();
		
	}
	
	public static void loginUser() {
		System.out.println("==============================================");
		System.out.println("*                                            *");
		System.out.println("*      Welcome to Login Page                 *");
		System.out.println("*                                            *");
		System.out.println("==============================================");
		
		System.out.println("Enter Username :");
		String inputusername=keyboard.next();
		boolean found = false;
		while(input.hasNext() && !found) {
			
			if(input.next().equals(inputusername)) {
				System.out.println("Enter Password");
				String inpPassword = keyboard.next();
				if(input.next().equals(inpPassword)) {
					System.out.println(("Login Successful"));
					found = true;
					lockerOptions(inputusername);
					break;
				}
				
			}
			
		}
		if(!found) {
			System.out.println("User Not found: Login failure Code 404!");
		}
		
		
	}
	

public static void fetchCredentials(String inpUsername)	{
	
	System.out.println("==============================================");
	System.out.println("*                                            *");
	System.out.println("*      Welcome to Digital Locker                 *");
	System.out.println("*          Your Creds Are                 *");
	System.out.println("*                                            *");
	System.out.println("==============================================");
	
	
	while(lockerInput.hasNext()) {
		
		if(lockerInput.next().equals(inpUsername)) {
			System.out.println("Site Name" + lockerInput.next());
			System.out.println("User Name" + lockerInput.next());
			System.out.println("Password" + lockerInput.next());
			
			
		}
		
	}
	
}
	
	
public static void lockerOptions(String inpUsername) {
		
		System.out.println("1.  Fetch ALL Stored Credentials");
		System.out.println("2.  Stored Credentials");
		int option = keyboard.nextInt();
		switch(option) {
		case 1 :
			fetchCredentials(inpUsername);
			break;
		case 2 :
			storeCredentials(inpUsername);
			break;
		default :
			System.out.println("Please Select 1 or 2");
			break;
		
		}
		
		
		keyboard.close();
		input.close();
		
	}
	
	
	public static void welcomeScreen() {
		
		System.out.println("==============================================");
		System.out.println("*                                            *");
		System.out.println("*      Welcome To LockMe.com                 *");
		System.out.println("*      Your Personal Digital Locker          *");
		System.out.println("*                                            *");
		System.out.println("==============================================");
		
		
	}
	
	public static void initApp() {
		
		File dbFile = new File("database.txt");
		File lockerFile = new File("locker-file.txt");
		
		try {
			input = new Scanner(dbFile);
			keyboard = new Scanner(System.in);
			
			lockerInput = new Scanner(lockerFile);
			
			output = new PrintWriter( new FileWriter(dbFile, true));
			lockerOutput = new PrintWriter( new FileWriter(lockerFile, true));
			
			users = new Users();
			userCredentials = new UserCredentials();
		} catch (IOException e) {
			
			System.out.println("404: File Not Found");
		}
		
		
	}
	
	public static void storeCredentials(String loggedInUser) {
        System.out.println("============================================");
                   System.out.println("*                                                                                                                                         ");
                   System.out.println("*        WELCOME TO DIGITAL LOCKER STORE YOUR CREDS HERE                             ");
                   System.out.println("*                                                                                                                                         ");
        System.out.println("============================================");
                   userCredentials.setLoggedInUser(loggedInUser);
                   
                   System.out.println("Enter Site Name:" );
                   String siteName=keyboard.next();
                   userCredentials.setSiteName(siteName);
                   
                   System.out.println("Enter User Name:" );
                   String username=keyboard.next();
                   userCredentials.setUsername(username);
                   
                   System.out.println("Enter Password:" );
                   String password=keyboard.next();
                   userCredentials.setPassword(password);
                   
                   lockerOutput.println(userCredentials.getLoggedInUser());
                   lockerOutput.println(userCredentials.getSiteName());
                   lockerOutput.println(userCredentials.getUsername());
                   lockerOutput.println(userCredentials.getPassword());
                   
                   System.out.println("YOUR CREDS ARE STORED AND SECURED!");
    
                   lockerOutput.close();
                   
    }

	
	
	public static void main(String[] args)  {
		
		initApp();
		welcomeScreen();
		signInOptions();
	}
}
